package com.example.rickmortyexample

import com.example.rickmortyexample.network.ApiService

class Repository(private val apiService: ApiService) {
    suspend fun getCharacters(page:String) = apiService.fetchCharacter(page)
}