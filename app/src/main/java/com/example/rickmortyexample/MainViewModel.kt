package com.example.rickmortyexample


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.rickmortyexample.network.ApiClient
import com.example.rickmortyexample.network.Character
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class MainViewModel(private val repository:Repository = Repository(ApiClient.apiService)) : ViewModel() {

    private var charactersLiveData = MutableLiveData<ScreenState<List<Character>?>>()
    val characterLiveData:LiveData<ScreenState<List<Character>?>>
    get() = charactersLiveData

    init{
        fetchCharacter()
    }

    private fun fetchCharacter(){

        charactersLiveData.postValue(ScreenState.Loading(null))

        viewModelScope.launch(Dispatchers.IO) {
            try{
                val client = repository.getCharacters("1")

                charactersLiveData.postValue(ScreenState.Success(client.result))

            }catch(e : Exception){
                charactersLiveData.postValue(ScreenState.Error(e.message.toString(),null))
            }
        }

    }
}